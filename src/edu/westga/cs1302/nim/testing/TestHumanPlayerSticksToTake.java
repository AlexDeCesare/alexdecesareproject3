package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.HumanPlayer;
import edu.westga.cs1302.nim.model.Pile;

class TestHumanPlayerSticksToTake {

	@Test
	public void shouldNotTakeAnySticksIfTheTotalSticksIsTheMinimumToWin() {
		
		Pile testPile = new Pile(1);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("the human player", testPile);
		
		testHumanPlayer.setSticksForTurn(1);

		assertThrows(IllegalArgumentException.class, () -> {
			testHumanPlayer.setSticksToTake();
		});
		
	}
	
	@Test 
	public void shouldTakeTwoSticksIfTotalSticksIsThree() {
		
		Pile testPile = new Pile(3);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("the human player", testPile);
		testHumanPlayer.setSticksForTurn(3);
		
		testHumanPlayer.setSticksToTake();
		
		assertEquals(2, testHumanPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsFour() {
		
		Pile testPile = new Pile(4);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("the human player", testPile);
		testHumanPlayer.setSticksForTurn(3);
		
		testHumanPlayer.setSticksToTake();
		
		assertEquals(3, testHumanPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsWellAboveFour() {
		
		Pile testPile = new Pile(400);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("the human player", testPile);
		testHumanPlayer.setSticksForTurn(3);
		
		testHumanPlayer.setSticksToTake();
		
		assertEquals(3, testHumanPlayer.getSticksToTake());
	}

}
