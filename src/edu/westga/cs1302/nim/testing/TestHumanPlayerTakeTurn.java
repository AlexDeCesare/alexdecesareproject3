package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.HumanPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestHumanPlayerTakeTurn {
	
	@Test
	public void shouldNotTakeTurnWhenSticksLeftIsAtMinimum() {
		
		Pile testPile = new Pile(1);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		assertThrows(IllegalArgumentException.class, () -> {
		
			testComputerPlayer.takeTurn();
		
		});
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsWellAboveMinimum() {
		
		Pile testPile = new Pile(20);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(3);

		testHumanPlayer.takeTurn();
		
		assertEquals(17, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsOneAboveMinimumThatCanBeTaken() {
		
		Pile testPile = new Pile(3);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(testPile.getSticksLeft());
		testHumanPlayer.setSticksToTake();

		testHumanPlayer.takeTurn();
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsWellAboveMinimumThatCanBeTaken() {
		
		Pile testPile = new Pile(4);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(testPile.getSticksLeft());
		testHumanPlayer.setSticksToTake();

		testHumanPlayer.takeTurn();
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test 
	public void shouldTakeTurnWhenSticksLeftIsAtWellAboveMaximumThatCanBeTaken() {
		Pile testPile = new Pile(5);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(testPile.getSticksLeft());
		testHumanPlayer.setSticksToTake();

		testHumanPlayer.takeTurn();
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test 
	public void shouldTakeTurnWhenSticksLeftIsAtOneAboveMaximumThatCanBeTaken() {
		Pile testPile = new Pile(6);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(testPile.getSticksLeft());
		testHumanPlayer.setSticksToTake();

		testHumanPlayer.takeTurn();
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test 
	public void shouldTakeTurnWhenSticksLeftIsWellAboveMaximumThatCanBeTaken() {
		Pile testPile = new Pile(300);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Player", testPile);
		testHumanPlayer.setSticksForTurn(testPile.getSticksLeft());
		testHumanPlayer.setSticksToTake();

		testHumanPlayer.takeTurn();
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}

}
