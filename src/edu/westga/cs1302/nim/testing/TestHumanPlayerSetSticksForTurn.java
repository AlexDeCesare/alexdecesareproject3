package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.HumanPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.Player;

class TestHumanPlayerSetSticksForTurn {

	@Test
	public void cannotSetSticksForTurnAtWellLessThanOne() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theHumanPlayer.setSticksForTurn(-500);
		});
		
	}
	
	@Test
	public void cannotSetSticksForTurnAtOneLessThanZero() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theHumanPlayer.setSticksForTurn(-1);
		});
		
	}
	
	@Test
	public void cannotSetSticksForTurnAtZero() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theHumanPlayer.setSticksForTurn(0);
		});
		
	}
	
	@Test
	public void shouldSetSticksForTurnAtOne() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);
		
		theHumanPlayer.setSticksForTurn(1);
	}
	
	@Test
	public void shouldSetSticksForTurnAtOneMoreThanOne() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);

		theHumanPlayer.setSticksForTurn(2);
		
		assertEquals(2, theHumanPlayer.getSticksToTake());	
	}
	
	@Test
	public void shouldSetSticksForTurnAtWellMoreThanOne() {
		
		Pile thePile = new Pile(7);
		Player theHumanPlayer = new HumanPlayer("Me", thePile);

		theHumanPlayer.setSticksForTurn(500);
		
		assertEquals(500, theHumanPlayer.getSticksToTake());
		
	}

}
