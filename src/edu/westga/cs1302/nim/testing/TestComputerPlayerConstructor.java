package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;

class TestComputerPlayerConstructor {

	@Test
	public void shouldNotAddComputerPlayerWithNullName() {
		
		Pile testPile = new Pile(7);
		
		assertThrows(IllegalArgumentException.class, () -> {
			new ComputerPlayer(null, testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddComputerPlayerWithEmptyName() {
		
		Pile testPile = new Pile(7);
		
		assertThrows(IllegalArgumentException.class, () -> {
			new ComputerPlayer("", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddComputerPlayerWithSticksWellNegative() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(-100);
			new ComputerPlayer("Computer player", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddComputerPlayerWithSticksOneBelowZero() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(-1);
			new ComputerPlayer("Computer player", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddComputerPlayerWithSticksAtZero() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(0);
			new ComputerPlayer("Computer player", testPile);
		});
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithLowerCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("computer", testPile);
		
		assertEquals("computer", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithLowerCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the computer player", testPile);
		
		assertEquals("the computer player", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithUpperCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("COMPUTER", testPile);
		
		assertEquals("COMPUTER", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithUpperCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("THE COMPUTER PLAYER", testPile);
		
		assertEquals("THE COMPUTER PLAYER", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithMultipleCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("Computer", testPile);
		
		assertEquals("Computer", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithMultipleCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", testPile);
		
		assertEquals("The Computer Player", testComputerPlayer.getName());
		
	}
	
	@Test
	public void shouldAddComputerPlayerWithSticksAtOne() {
		
		Pile testPile = new Pile(1);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("Computer Player", testPile);
		
		assertEquals(1, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldAddComputerPlayerWithSticksWellAboveOne() {
		
		Pile testPile = new Pile(500);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("Computer Player", testPile);
		
		assertEquals(500, testComputerPlayer.getPile().getSticksLeft());
	}

}
