package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;

class TestSetPileForThisTurn {

	@Test
	public void shouldNotSetNullPile() {
		Game testGame = new Game();
		
		assertThrows(IllegalArgumentException.class, () -> {
			testGame.setPileForThisTurn(null);
		});
	}
	
	@Test
	public void shouldSetDefaultPileAtSeven() {
		Game testGame = new Game();

		assertEquals(7, testGame.getPile().getSticksLeft());

	}
	
	@Test
	public void shouldSetPileForThisTurnAtOne() {
		Game testGame = new Game();
		Pile testPile = new Pile(1);
		
		testGame.setPileForThisTurn(testPile);

		assertEquals(1, testGame.getPile().getSticksLeft());

	}
	
	@Test
	public void shouldSetPileForThisTurnWellAboveOne() {
		Game testGame = new Game();
		Pile testPile = new Pile(400);
		
		testGame.setPileForThisTurn(testPile);

		assertEquals(400, testGame.getPile().getSticksLeft());

	}

}
