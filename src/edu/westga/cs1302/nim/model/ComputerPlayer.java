package edu.westga.cs1302.nim.model;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class defines the computer player
 * 
 * @author Alex DeCesare
 * @version 11-July-2020
 */

public class ComputerPlayer extends Player {
	
	private static final int MINIMUM_NUMBER_OF_STICKS_LEFT = 1;
	
	private NumberOfSticksStrategy theNumberOfSticksStrategy;

	/**
	 * The constructor for the computer player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param name the name of the player
	 * @param thePileOfSticks the sticks of the player
	 */
	
	public ComputerPlayer(String name, Pile thePileOfSticks) {
		super(name, thePileOfSticks);
	}
	
	/**
	 * Sets the default sticks to take for the human player
	 * 
	 * @precondition super.getPile().getSticksLeft() >= 1
	 * @postcondition none
	 */

	@Override
	public void setSticksToTake() {

		int theSticksToTake;
		try {
			
			if (this.getPile().getSticksLeft() < MINIMUM_NUMBER_OF_STICKS_LEFT) {
				throw new IllegalArgumentException(ErrorMessages.CANNOT_REDUCE_STICKS_FOR_PLAYER_TO_LESS_THAN_MINIMUM);
			}
			
			theSticksToTake = this.getStrategy().howManySticks(this.getPile().getSticksLeft());
			
			super.setSticksForTurn(theSticksToTake);
		} catch (NullPointerException theNullPointerException) {
			Alert theAlert = new Alert(AlertType.ERROR);
			theAlert.setContentText(ErrorMessages.CANNOT_SET_STICKS_TO_TAKE_WITHOUT_A_STRATEGY);
			theAlert.showAndWait();
		}
		
	}
	
	/**
	 * Sets the strategy
	 * 
	 * @precondition theStrategy != null
	 * @postcondition none
	 * 
	 * @param theStrategy the strategy to set
	 */
	
	public void setSticksStrategy(NumberOfSticksStrategy theStrategy) {
		
		if (theStrategy == null) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_A_NULL_STRATEGY);
		}
		
		this.theNumberOfSticksStrategy = theStrategy;
	}
	
	/**
	 * Gets the strategy
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the strategy
	 */
	
	public NumberOfSticksStrategy getStrategy() {
		return this.theNumberOfSticksStrategy;
	}
	
	/**
	 * The toString method for the computer player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the computer player
	 */
	
	@Override
	public String toString() {
		return "A computer player with " + super.getSticksToTake() + " sticks to take and a name of " + super.getName();
	}
}
