package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;

class TestComputerPlayerSetCurrentPile {
	
	@Test
	public void shouldNotSetANullPile() {
		Pile initialPile = new Pile(7);
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", initialPile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testComputerPlayer.setCurrentPile(null);
		});
		
	}
	
	@Test
	public void shouldSetTheNewPileThatIsOneAboveZero() {
		
		Pile initialPile = new Pile(7);
		Pile newPile = new Pile(1);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", initialPile);
		testComputerPlayer.setCurrentPile(newPile);
		assertEquals(1, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileThatIsWellAboveZero() {
		
		Pile initialPile = new Pile(7);
		Pile newPile = new Pile(500);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", initialPile);
		testComputerPlayer.setCurrentPile(newPile);
		assertEquals(500, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileWhenInitialPileIsWellAboveZero() {
		
		Pile initialPile = new Pile(500);
		Pile newPile = new Pile(7);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", initialPile);
		testComputerPlayer.setCurrentPile(newPile);
		assertEquals(7, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileWhenInitialPileIsOneAboveZero() {
		
		Pile initialPile = new Pile(1);
		Pile newPile = new Pile(50);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Computer Player", initialPile);
		testComputerPlayer.setCurrentPile(newPile);
		assertEquals(50, testComputerPlayer.getPile().getSticksLeft());
	}

}
