package edu.westga.cs1302.nim.model.strategy;

/**
 * The interface for the number of sticks strategy for the computer class
 * 
 * @author Alex DeCesare
 * @version 12-July-2020
 */

public interface NumberOfSticksStrategy {

	/**
	 * returns the number of sticks to be taken from the pile
	 * 
	 * @precondition numberOfSticks >= MINIMUM_NUMBER_OF_STICKS_FOR_GAME
	 * @postcondition none
	 * 
	 * @param pileSize the size of the pile
	 * 
	 * @return the number of sticks
	 */
	
	int howManySticks(int pileSize);
	
}
