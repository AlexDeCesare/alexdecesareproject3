package edu.westga.cs1302.nim.model.strategy.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.GreedyStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestSetStrategy {

	@Test
	public void shouldNotAddNullStrategy() {
		
		Pile thePile = new Pile(7);
		ComputerPlayer theComputerPlayer = new ComputerPlayer("Computer Player", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theComputerPlayer.setSticksStrategy(null);
		});
		
	}
	
	@Test
	public void shouldSetCautiousStrategy() {
		
		Pile thePile = new Pile(7);
		ComputerPlayer theComputerPlayer = new ComputerPlayer("Computer Player", thePile);
		NumberOfSticksStrategy theCautiousStrategy = new CautiousStrategy();
		
		theComputerPlayer.setSticksStrategy(theCautiousStrategy);
		
		assertEquals("The cautious strategy", theCautiousStrategy.toString());
	}
	
	@Test
	public void shouldSetGreedyStrategy() {
		
		Pile thePile = new Pile(7);
		ComputerPlayer theComputerPlayer = new ComputerPlayer("Computer Player", thePile);
		NumberOfSticksStrategy theGreedyStrategy = new GreedyStrategy();
		
		theComputerPlayer.setSticksStrategy(theGreedyStrategy);
		
		assertEquals("The greedy strategy", theGreedyStrategy.toString());
	}

}
