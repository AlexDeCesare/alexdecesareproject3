package edu.westga.cs1302.nim.model;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * This class defines the game
 * 
 * @author Alex DeCesare
 * @version 11-July-2020
 */

public class Game {
	
	private static final int MINIMUM_NUMBER_OF_STICKS_LEFT = 1;
	private static final int NUMBER_OF_STICKS_TO_PLAY_GAME = 7;
	
	private static final String NAME_OF_HUMAN_PLAYER = "Me";
	private static final String NAME_OF_COMPUTER_PLAYER = "Computer";
	
	private HumanPlayer theHumanPlayer;
	private ComputerPlayer theComputerPlayer;
	private Pile thePile;
	private Player theCurrentPlayer;
	private Player theStandByPlayer;
	
	/**
	 * This is the constructor for the game
	 * 
	 * @precondition none
	 * @postcondition thePile == getPile && theHumanPlayer == getHumanPlayer && theComputerPlayer == getComputerPlayer 
	 * 				  && theCurrentPlayer == theHumanPlayer && theStandByPlayer = theComputerPlayer
	 */
	
	public Game() {
		
		this.thePile = new Pile(NUMBER_OF_STICKS_TO_PLAY_GAME);
		this.theHumanPlayer = new HumanPlayer(NAME_OF_HUMAN_PLAYER, this.thePile);
		this.theComputerPlayer = new ComputerPlayer(NAME_OF_COMPUTER_PLAYER, this.thePile);
		this.theCurrentPlayer = this.theHumanPlayer;
		this.theStandByPlayer = this.theComputerPlayer;
	}

	/**
	 * Gets the human player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the human player
	 */
	
	public HumanPlayer getHumanPlayer() {
		return this.theHumanPlayer;
	}
	
	/**
	 * Gets the computer player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the computer player
	 */
	
	public ComputerPlayer getComputerPlayer() {
		return this.theComputerPlayer;
	}
	
	/**
	 * Gets the pile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the pile used for the game
	 */
	
	public Pile getPile() {
		return this.thePile;
	}
	
	/**
	 * Gets the current player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the current player
	 */
	
	public Player getCurrentPlayer() {
		return this.theCurrentPlayer;
	}
	
	/**
	 * Gets the stand by player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the stand by player
	 */
	
	public Player getStandByPlayer() {
		return this.theStandByPlayer;
	}
	
	/**
	 * gets the sticks on the pile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sticks on the current pile
	 */
	
	public int getSticksOnPile() {
		return this.getPile().getSticksLeft();
	}
	
	/**
	 * Sets the pile for this turn
	 * 
	 * @precondition pileToSet != null
	 * @postcondition none
	 *
	 * @param pileToSet the pile to set for this turn
	 */
	
	public void setPileForThisTurn(Pile pileToSet) {
		
		if (pileToSet == null) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_A_NULL_PILE);
		}
		
		this.thePile = pileToSet;
		this.getComputerPlayer().setCurrentPile(pileToSet);
		this.getHumanPlayer().setCurrentPile(pileToSet);
	}

	/**
	 * Checks if the game is over by determining if sticks are below minimum sticks
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if game is finished and false if it is not
	 */
	
	public Boolean isGameOver() {
		
		return this.getPile().getSticksLeft() == MINIMUM_NUMBER_OF_STICKS_LEFT;
		
	}
	
	/**
	 * Plays the game by taking the current player turn and switching the turns
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public void play() {
		this.getCurrentPlayer().takeTurn();
		this.swapWhoseTurn();
	}
	
	/**
	 * Returns a string representation of the game
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the game
	 */
	
	@Override
	public String toString() {
		
		if (this.isGameOver()) {
			return "The winner of the game is " + this.getStandByPlayer().getName();
		} else {
			return "Game still in progress the number of sticks left is: " + this.getPile().getSticksLeft();
 		}
		
	}
	
	private void swapWhoseTurn() {
		
		Player theTemporaryPlayer = this.getCurrentPlayer();
		this.theCurrentPlayer = this.getStandByPlayer();
		this.theStandByPlayer = theTemporaryPlayer;
		
	}
	
}
