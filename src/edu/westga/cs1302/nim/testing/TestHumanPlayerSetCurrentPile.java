package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.HumanPlayer;
import edu.westga.cs1302.nim.model.Pile;

class TestHumanPlayerSetCurrentPile {
	
	@Test
	public void shouldNotSetANullPile() {
		Pile initialPile = new Pile(7);
		HumanPlayer testHumanPlayer = new HumanPlayer("The Computer Player", initialPile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testHumanPlayer.setCurrentPile(null);
		});
		
	}

	@Test
	public void shouldSetTheNewPileThatIsOneAboveZero() {
		
		Pile initialPile = new Pile(7);
		Pile newPile = new Pile(1);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Human Player", initialPile);
		testHumanPlayer.setCurrentPile(newPile);
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileThatIsWellAboveZero() {
		
		Pile initialPile = new Pile(7);
		Pile newPile = new Pile(500);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Human Player", initialPile);
		testHumanPlayer.setCurrentPile(newPile);
		assertEquals(500, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileWhenInitialPileIsWellAboveZero() {
		
		Pile initialPile = new Pile(500);
		Pile newPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Human Player", initialPile);
		testHumanPlayer.setCurrentPile(newPile);
		assertEquals(7, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldSetTheNewPileWhenInitialPileIsOneAboveZero() {
		
		Pile initialPile = new Pile(1);
		Pile newPile = new Pile(50);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Human Player", initialPile);
		testHumanPlayer.setCurrentPile(newPile);
		assertEquals(50, testHumanPlayer.getPile().getSticksLeft());
	}
}
