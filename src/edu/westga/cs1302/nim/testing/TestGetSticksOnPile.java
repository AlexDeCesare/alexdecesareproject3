package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;

class TestGetSticksOnPile {

	@Test
	public void shouldGetDefaultSticksOnPile() {
		Game theGame = new Game();
		
		assertEquals(7, theGame.getSticksOnPile());
	}
	
	@Test
	public void shouldGetSticksOnPileWhenThereAreSticksOneAboveMinimum() {
		Game theGame = new Game();
		Pile thePile = new Pile(3);
		theGame.setPileForThisTurn(thePile);
		
		assertEquals(3, theGame.getSticksOnPile());
	}
	
	@Test
	public void shouldGetSticksOnPileWhenThereAreSticksWellAboveMinimum() {
		Game theGame = new Game();
		Pile thePile = new Pile(300);
		theGame.setPileForThisTurn(thePile);
		
		assertEquals(300, theGame.getSticksOnPile());
	}

}
