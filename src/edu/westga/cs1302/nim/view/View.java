package edu.westga.cs1302.nim.view;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;
import edu.westga.cs1302.nim.model.Game;

/**
 * The view class for the project
 * 
 * @author Alex DeCesare
 * @version 13-July-2020
 */

public class View {
	
	/**
	 * The constructor for the view class
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public View() {
		
	}
	
	/**
	 * Gets the description of a given game
	 * 
	 * @precondition theGame != null
	 * @postcondition none
	 * 
	 * @param theGame the game to get the description of
	 * 
	 * @return the description of a given game
	 */
	
	public String getGameDescription(Game theGame) {
		
		if (theGame == null) {
			throw new IllegalArgumentException(ErrorMessages.THE_GAME_TO_GET_THE_DESCRIPTION_OF_CANNOT_BE_NULL);
		}
		
		return theGame.toString();
	}

}
