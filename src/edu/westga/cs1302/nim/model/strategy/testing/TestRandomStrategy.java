package edu.westga.cs1302.nim.model.strategy.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.RandomStrategy;

class TestRandomStrategy {

	@Test
	public void shouldNotReturnForSticksWellLessThanOne() {
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theRandomStrategy.howManySticks(-300);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtOneBelowZero() {
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theRandomStrategy.howManySticks(-1);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtZero() {
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theRandomStrategy.howManySticks(0);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtMinimum() {
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theRandomStrategy.howManySticks(1);
		});
	}
	
	@Test
	public void shouldFindRandomNumberBetweenMinimumSticksAndOneAboveMinimum() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(2);
		theGame.setPileForThisTurn(thePile);
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		theGame.getComputerPlayer().setSticksStrategy(theRandomStrategy);
		theGame.getComputerPlayer().setSticksToTake();
		
		int theSticks = theGame.getComputerPlayer().getSticksToTake();
		
		boolean isBetween = (1 <= theSticks || theSticks >= 2);
		
		assertEquals(true, isBetween);
		
	}
	
	@Test
	public void shouldFindRandomNumberBetweenMinimumSticksOneBelowDefaultSticks() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(6);
		theGame.setPileForThisTurn(thePile);
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		theGame.getComputerPlayer().setSticksStrategy(theRandomStrategy);
		theGame.getComputerPlayer().setSticksToTake();
		
		int theSticks = theGame.getComputerPlayer().getSticksToTake();
		
		boolean isBetween = 1 <= theSticks || theSticks >= 5;
		
		assertEquals(true, isBetween);
		
	}
	
	@Test
	public void shouldFindRandomNumberBetweenMinimumSticksDefaultSticks() {
		
		Game theGame = new Game();
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		theGame.getComputerPlayer().setSticksStrategy(theRandomStrategy);
		theGame.getComputerPlayer().setSticksToTake();
		
		int theSticks = theGame.getComputerPlayer().getSticksToTake();
		
		boolean isBetween = 1 <= theSticks || theSticks >= 6;
		
		assertEquals(true, isBetween);
		
	}
	
	@Test
	public void shouldFindRandomNumberBetweenMinimumSticksOneAboveDefaultSticks() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(8);
		theGame.setPileForThisTurn(thePile);
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		theGame.getComputerPlayer().setSticksStrategy(theRandomStrategy);
		theGame.getComputerPlayer().setSticksToTake();
		
		int theSticks = theGame.getComputerPlayer().getSticksToTake();
		
		boolean isBetween = 1 <= theSticks || theSticks >= 7;
		
		assertEquals(true, isBetween);
		
	}
	
	@Test
	public void shouldFindRandomNumberBetweenMinimumSticksAndSticksWellAboveMinimum() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(200);
		theGame.setPileForThisTurn(thePile);
		
		RandomStrategy theRandomStrategy = new RandomStrategy();
		theGame.getComputerPlayer().setSticksStrategy(theRandomStrategy);
		theGame.getComputerPlayer().setSticksToTake();
		
		int theSticks = theGame.getComputerPlayer().getSticksToTake();
		
		boolean isBetween = 1 <= theSticks || theSticks >= 200;
		
		assertEquals(true, isBetween);
		
	}

}
