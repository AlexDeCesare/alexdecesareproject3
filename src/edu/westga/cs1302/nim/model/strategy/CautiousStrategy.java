package edu.westga.cs1302.nim.model.strategy;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * Defines the cautious strategy for the computer player
 * 
 * @author Alex DeCesare
 * @version 12-July-2020
 */

public class CautiousStrategy implements NumberOfSticksStrategy {
	
	private static final int MINIMUM_NUMBER_OF_STICKS_LEFT = 1;
	private static final int MINIMUM_NUMBER_OF_STICKS_TO_TAKE = 1;
	
	/**
	 * returns the number of sticks to be taken from the pile
	 * 
	 * @precondition numberOfSticks >= MINIMUM_NUMBER_OF_STICKS_FOR_GAME
	 * @postcondition none
	 * 
	 * @param pileSize the size of the pile
	 * 
	 * @return the minimum number of sticks to take which is one
	 */
	
	@Override
	public int howManySticks(int pileSize) {
		
		if (pileSize <= MINIMUM_NUMBER_OF_STICKS_LEFT) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_STRATEGY_WITH_STICKS_LESS_THAN_MINIMUM);
		}

		return MINIMUM_NUMBER_OF_STICKS_TO_TAKE;
	}
	
	/**
	 * Returns the string representation of the cautious strategy
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the cautious strategy
	 */
	
	@Override
	public String toString() {
		return "The cautious strategy";
	}

}
