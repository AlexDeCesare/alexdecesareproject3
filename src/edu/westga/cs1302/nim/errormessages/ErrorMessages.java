package edu.westga.cs1302.nim.errormessages;

/**
 * This class holds the error messages
 * 
 * @author Alex DeCesare
 * @version 110-July-2020
 */

public class ErrorMessages {

	public static final String NUMBER_OF_STICKS_CANNOT_BE_LESS_THAN_OR_EQUAL_TO_ZERO = "The number of sticks cannot be less than or equal to zero";
	public static final String NUMBER_OF_STICKS_TO_REMOVE_CANNOT_BE_ZERO_OR_LESS = "The number of sticks to remove from the pile cannot be less than or equal to zero";
	public static final String NUMBER_OF_STICKS_TO_REMOVE_CANNOT_MAKE_NEGATIVE_TOTAL_STICKS = "Too many sticks removed, cannot remove more sticks than there is in the pile";
	public static final String CANNOT_REDUCE_STICKS_FOR_PLAYER_TO_LESS_THAN_MINIMUM = "The sticks left for the player cannot be reduced to less than minimum";
	
	public static final String THE_PLAYER_NAME_CANNOT_BE_NULL = "The name of the player cannot be null";
	public static final String THE_PLAYER_NAME_CANNOT_BE_EMPTY = "The name of the player cannot be empty, please enter a name";

	public static final String CANNOT_SET_A_NULL_PILE = "Cannot set a null pile";
	
	public static final String CANNOT_SET_STRATEGY_WITH_STICKS_LESS_THAN_MINIMUM = "Cannot set a strategy with sticks less than minimum";
	public static final String CANNOT_SET_A_NULL_STRATEGY = "Cannot set a null strategy";
	
	public static final String CANNOT_SET_STICKS_TO_TAKE_WITHOUT_A_STRATEGY = "Cannot set the sticks to take without a strategy";
	public static final String CANNOT_SET_STICKS_FOR_TURN_AT_LESS_THAN_MINIMUM = "Cannot set the sticks to take to less than the minimum sticks to take";
	
	public static final String DEFINE_THE_STICKS_TO_TAKE = "Please define the sticks to take before getting the sticks taken";
	
	public static final String THE_GAME_TO_GET_THE_DESCRIPTION_OF_CANNOT_BE_NULL = "The game to get the description of cannot be null";
}
