package edu.westga.cs1302.nim.model;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * Models the pile of sticks
 * 
 * @author Alex DeCesare
 * @version 10-July-2020
 */

public class Pile {
	
	private int numberOfSticks;
	
	/**
	 * The constructor for the pile
	 * 
	 * @precondition numberOfSticks >= MINIMUM_NUMBER_OF_STICKS_FOR_GAME
	 * @postcondition numberOfSticks == getNumberOfSticks()
	 * 
	 * @param numberOfSticks the number of sticks in the pile
	 */
	
	public Pile(int numberOfSticks) {
		
		if (numberOfSticks <= 0) {
			throw new IllegalArgumentException(ErrorMessages.NUMBER_OF_STICKS_CANNOT_BE_LESS_THAN_OR_EQUAL_TO_ZERO);
		}
		
		this.numberOfSticks = numberOfSticks;
		
	}
	
	/**
	 * removes sticks from the pile
	 * 
	 * @precondition sticksToRemove > 0 && getNumberOfSticks() - sticksToRemove >= 0
	 * @postcondition numberOfSticks == getNumberOfSticks() - sticksToRemove
	 * 
	 * @param sticksToRemove the amount of sticks to remove from the pile
	 */
	
	public void removeSticks(int sticksToRemove) {
		
		if (sticksToRemove <= 0) {
			throw new IllegalArgumentException(ErrorMessages.NUMBER_OF_STICKS_TO_REMOVE_CANNOT_BE_ZERO_OR_LESS);
		}
		if ((this.numberOfSticks - sticksToRemove) < 0) {
			throw new IllegalArgumentException(ErrorMessages.NUMBER_OF_STICKS_TO_REMOVE_CANNOT_MAKE_NEGATIVE_TOTAL_STICKS);
		}
		
		this.numberOfSticks -= sticksToRemove;
		
	}
	
	/**
	 * gets the sticks left
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of sticks 
	 */
	
	public int getSticksLeft() {
		return this.numberOfSticks;
	}
	
	/**
	 * The toString method for the pile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the pile
	 */
	
	@Override
	public String toString() {
		
		return "A pile with " + this.numberOfSticks + " sticks";
		
	}
	
}
