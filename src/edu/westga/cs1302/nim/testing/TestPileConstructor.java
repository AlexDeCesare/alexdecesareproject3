package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Pile;

class TestPileConstructor {

	@Test
	public void shouldNotAllowSticksWellBelowZero() {

		assertThrows(IllegalArgumentException.class, () -> {
			new Pile(-500);
		});
		
	}
	
	@Test
	public void shouldNotAllowSticksOneBelowZero() {

		assertThrows(IllegalArgumentException.class, () -> {
			new Pile(-1);
		});
		
	}
	
	@Test
	public void shouldNotAllowSticksAtZero() {

		assertThrows(IllegalArgumentException.class, () -> {
			new Pile(0);
		});
		
	}
	
	@Test
	public void shouldAllowSticksOneAboveZero() {

		Pile testPile = new Pile(1);
		
		assertEquals("A pile with 1 sticks", testPile.toString());
		
	}
	
	@Test
	public void shouldAllowSticksWellAboveZero() {

		Pile testPile = new Pile(500);
		
		assertEquals("A pile with 500 sticks", testPile.toString());
		
	}

}
