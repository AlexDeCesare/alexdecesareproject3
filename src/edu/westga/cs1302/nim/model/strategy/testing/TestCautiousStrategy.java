package edu.westga.cs1302.nim.model.strategy.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;

class TestCautiousStrategy {

	@Test
	public void shouldNotReturnForSticksWellLessThanOne() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theCautiousStrategy.howManySticks(-300);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtOne() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theCautiousStrategy.howManySticks(-1);
		});
	}

	@Test
	public void shouldNotReturnForSticksAtOneBelowMinimum() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theCautiousStrategy.howManySticks(0);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtMinimum() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theCautiousStrategy.howManySticks(1);
		});
	}
	
	@Test
	public void shouldReturnOneForSticksOneAboveMinimum() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
	
		assertEquals(1, theCautiousStrategy.howManySticks(2));
		
	}
	
	@Test
	public void shouldReturnOneForSticksWellAboveMinimum() {
		
		CautiousStrategy theCautiousStrategy = new CautiousStrategy();
	
		assertEquals(1, theCautiousStrategy.howManySticks(300));
		
	}

}
